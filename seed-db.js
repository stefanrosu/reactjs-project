"use strict";
exports.__esModule = true;
var fs = require("fs");
var faker = require("faker");
var dbFileName = 'db.json';
var maxUsers = 10;
var maxBooks = 5;
var maxCourses = 5;
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());
var Book = /** @class */ (function () {
    function Book() {
    }
    return Book;
}());
var Course = /** @class */ (function () {
    function Course() {
    }
    return Course;
}());
var DB = /** @class */ (function () {
    function DB() {
    }
    return DB;
}());
var SeedDB = /** @class */ (function () {
    function SeedDB() {
        this.db = new DB();
        this.db.users = [];
        this.db.books = [];
        this.db.courses = [];
    }
    SeedDB.prototype.randomDate = function (start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    };
    SeedDB.prototype.seedUsers = function (usersNumber) {
        for (var index = 1; index <= usersNumber; index++) {
            var user = {
                "id": index,
                "firstName": faker.name.firstName(),
                "lastName": faker.name.lastName(),
                "email": faker.internet.email(),
                "password": faker.internet.password(),
                "hobby": faker.random.words(),
                "age": Math.floor((Math.random() * 70) + 18)
            };
            this.db.users.push(user);
        }
    };
    SeedDB.prototype.seedBooks = function (booksNumber) {
        var _this = this;
        var bookId = 1;
        this.db.users.forEach(function (user) {
            for (var index = 1; index <= booksNumber; index++) {
                var book = {
                    "id": bookId,
                    "userId": user.id,
                    "title": faker.lorem.words(),
                    "author": faker.name.findName(),
                    "published": faker.date.past(),
                    "publisher": faker.company.companyName(),
                    "pages": Math.floor((Math.random() * 1000) + 10),
                    "description": faker.lorem.paragraphs(),
                    "website": faker.internet.url(),
                    "images": faker.image.imageUrl()
                };
                _this.db.books.push(book);
                bookId++;
            }
        });
    };
    SeedDB.prototype.seedCourses = function (coursesNumber) {
        var _this = this;
        var courseId = 1;
        this.db.books.forEach(function (book) {
            for (var index = 1; index <= coursesNumber; index++) {
                var course = {
                    "id": courseId,
                    "bookId": book.id,
                    "small_description": faker.lorem.sentence(),
                    "long_description": faker.lorem.paragraphs(),
                    "tags": faker.random.word(),
                    "points": Math.floor((Math.random() * 20) + 1),
                    "images": faker.image.imageUrl()
                };
                _this.db.courses.push(course);
                courseId++;
            }
        });
    };
    SeedDB.prototype.seedDB = function () {
        this.seedUsers(maxUsers);
        this.seedBooks(maxBooks);
        this.seedCourses(maxCourses);
        var json = JSON.stringify(this.db);
        fs.writeFile(dbFileName, json, 'utf8', function (err) {
            if (err)
                throw err;
            console.log('Seed complete!');
        });
    };
    return SeedDB;
}());
exports["default"] = SeedDB;
;
var seed = new SeedDB();
seed.seedDB();
