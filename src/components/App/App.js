import React, { Component } from 'react';
import './App.scss';
import Main from '../main-routes'
import 'font-awesome/css/font-awesome.min.css';
import HeaderItem from '../public-components/header-component';
import Footer from '../public-components/footer-component';

class App extends Component {

  render() {

    return (
      <div className="App">
        < HeaderItem />
        <Main />
      <div>
        <Footer/>
      </div>
      </div>
    );
  }
}

export default App;
