import React, { Component } from 'react';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import './map.scss'

export class MapContainer extends Component {

  render() {
    const google = window.google = window.google ? window.google : {}
    return (
      <Map google={this.props.google}
        zoom={15}
        initialCenter={{ lat: 47.640942, lng: 26.246441 }}
      >
        <Marker onClick={this.onMarkerClick}
          name={'Current location'}
          icon={{
                  url: "https://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi2.png",
                  anchor: new google.maps.Point(20,20),
                  scaledSize: new google.maps.Size(25,40)
          }}
        />
      </Map>
    );
  }
}

export default GoogleApiWrapper({
  // apiKey: ('AIzaSyBvz2DK1-zMF3N5AkjnjdlnKUH1xswRDoo')
  apiKey: ('AIzaSyAHbKWGMwqv0GJhaVj1XKMXFZeLdzzXK')
})(MapContainer)
