import React, { Component } from 'react';
import MapContainer from './map-component';
import { Link } from 'react-router-dom';
import '../home-component/home.scss'
import {
  Container,
  Row,
  Col,
} from 'reactstrap';

class Location extends Component {
  render() {
    return (
      <>
        <Container>
          <Row>
            <Col md="12" className="center-position">
              <div className="map-container">
                <MapContainer />
              </div>
            </Col>
          </Row>
          <Row>
            <Col md="12" className="center-position">
              <Link to={'/'} >
                <button className="btn btn-primary" >Back to homepage</button>
              </Link></Col>
          </Row>
       </Container>
      </>
    );
  }
}
export default Location;
