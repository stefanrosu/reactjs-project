import React, { Component } from 'react'
import './footer.scss'
import {NavLink} from 'react-router-dom'

class Footer extends Component {
  constructor(props){
      super(props)
      this.state = {
      }
  }

  render(){
    return (
      <div className='footer-container'>
        <div className="footer-menu">
          <NavLink exact to="/" className="main-nav" activeClassName="main-nav-active" >
            <i className="fa fa-home"></i>
          </NavLink>
          <NavLink exact to="/books" className="main-nav" activeClassName="main-nav-active" >
            <i className="fa fa-book"></i>
          </NavLink>
          <NavLink exact to="/courses" className="main-nav" activeClassName="main-nav-active" >
            <i className="fa fa-graduation-cap"></i>
          </NavLink>
          <NavLink exact to="/location" className="main-nav" activeClassName="main-nav-active" >
            <i className="fa fa-map-marker"></i>
          </NavLink>
          <NavLink exact to="/users" className="main-nav" activeClassName="main-nav-active" >
            <i className="fa fa-users"></i>
          </NavLink>
        </div>
        <div className="top-menu" >
          <a href="#top">
            <i className="fa fa-chevron-up"></i>
          </a>
        </div>
      </div>
    )
  }
}


export default Footer
