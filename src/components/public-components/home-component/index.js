import React, { Component } from 'react';
import './home.scss'
import NewUser from '../new-user-component';
import {
  Container,
  Row,
  Col,
} from 'reactstrap';

class Home extends Component {

  render() {
    return (
      <>
        <Container>
            <Row>
              <Col md="12" className="center-position">
               <NewUser/>
              </Col>
            </Row>
        </Container>
      </>
    );
  }
}
export default Home;
