import React, { Component } from 'react'
import './books.scss'
import route from '../../api-services/index';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import TextTruncate from 'react-text-truncate';

import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
  Form,
  Input,
  InputGroup,
} from 'reactstrap';

class Books extends Component {
  constructor(props){
    super(props)
    this.state = {
      allBooks: [],
      minPage: 0,
      maxPage: 4,
      searchValue: '',
      searchErrMessage: '',
    }
  }

  componentDidMount = () => {
    axios.get(route.baseURL + route.booksLimitTo + '_start='+ this.state.minPage + '&_end=' + this.state.maxPage)
    .then(res => {
      this.setState({
        allBooks: res.data,
      })
    })
  }

  getMore = () => {
    this.setState({
      maxPage: this.state.maxPage + 4,
    }, () => {
      this.componentDidMount();
    })
  }

  getLess = () => {
    if (this.state.maxPage > 4) {
      this.setState({
        maxPage: this.state.maxPage - 4,
      }, () => {
        this.componentDidMount();
      })
    }
  }

  handleSearchChange = (event) => {
    this.setState({
      searchValue: event.target.value,
      searchErrMessage: '',
    })
  }

  searchField = () => {
    if (this.state.searchValue !== '') {
      axios.get(route.baseURL + route.booksLimitTo + '&q=' + this.state.searchValue)
      .then(res => {
        this.setState({
          allBooks: res.data,
        })
      })
    }
    else {
      this.setState({
        searchErrMessage: 'Please enter a value before search!'
      })
    }
  }

  resetSearchField = () => {
    this.setState({
      searchValue: '',
    }, () => {
      axios.get(route.baseURL + route.booksLimitTo + '&q=' + this.state.searchValue)
      .then(res => {
        this.setState({
          allBooks: res.data,
          searchErrMessage: '',
        })
      })
      document.getElementById('searchForm').reset()
    })
  }

  render() {
    const { allBooks, maxPage } = this.state;
        return (
            <>
            <Container className="custom-container">
              <Row>
                <Col md="12" className="center-position" >
                  <h2>Find your favorite book</h2>
                  <br />
                  <Form id="searchForm" onSubmit={this.searchField} >
                <Row>
                  <Col md="7">
                  <InputGroup>
                      <Input type="search" placeholder="... type to search" name="searchValue" onChange={this.handleSearchChange} />
                    </InputGroup>
                  <div className="required_field">{ this.state.searchErrMessage }</div>
                  </Col>
                  <Col md="5" >
                    <Button color="primary" size="md" onClick={ ()=> this.searchField() } >Find your book</Button>
                    <Button size="md" onClick={()=> this.resetSearchField()} >Reset</Button>
                  </Col>
                </Row>
              </Form>
                </Col>
              </Row>
              <hr />
              <Row>
                {allBooks.map((item) => (
                <Col md="6" key={item.id}>
                  <Card >
                  <CardImg top width="300px" height="300px" src={item.images} alt="Card image" />
                  <CardBody>
                    <CardTitle>
                      <b>Title:</b> {item.title}
                    </CardTitle>
                    <div>
                      <b>Description:</b>
                      <TextTruncate
                      line={5}
                      truncateText="…"
                      text={item.description}

                      />
                    </div>
                    <CardText>
                      <b>Author:</b> {item.author}
                    </CardText>
                    <CardText>
                      <b>Publisher:</b> {item.publisher}
                    </CardText>
                    <CardText>
                      <b>Published: </b>
                      <Moment format="DD/MM/YYYY">
                        {item.published}
                      </Moment>
                    </CardText>
                    <CardText>
                      <b>Pages:</b> {item.pages}
                    </CardText>
                    <CardText>
                      <b>Website: </b>
                      <a href={item.website} target="_blank" rel="noopener noreferrer">{item.website}</a>
                    </CardText>
                      <Link to={route.courses + item.id}> Click to view all courses of this book
                      </Link>
                  </CardBody>
                  </Card>
                  <br/>
               </Col>
                ))}
              </Row>
              <hr/>
              <Row>
                <Col md="12" className="center-position">
                  <Button color="success" onClick={() => {
                    this.getMore()
                    }} >Show more </Button>
                {maxPage > 4 && (
                  <Button color="danger" onClick={() => {
                    this.getLess()
                    }} >Show less </Button>
                )}
                </Col>
              </Row>
              <br/>
            </Container>
            </>
        )
    }
}

export default Books;
