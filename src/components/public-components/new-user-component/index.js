import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import route from '../../api-services/index';
import '../home-component/home.scss'
import { Container, Row, Col, Button, Input, InputGroup, InputGroupAddon, Form, } from 'reactstrap';
import UserClass from '../../classes/userClass';
class NewUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userObj: UserClass,
      fnameErr: '',
      lnameErr: '',
      emailErr: '',
      passwordErr: '',
      redirectAfterSubmit: false,
      terms: false,
      condition: false,
    }
  }

  handleChange = e => {
    this.setState({
      userObj: {
        ...this.state.userObj,
        [e.target.name]: e.target.value
      },
      [e.target.name]: e.target.checked
    })
  }

  validate = () => {
    let fnameErr = '';
    let lnameErr = '';
    let emailErr = '';
		let passwordErr = '';

		let onlyLetterRegex = RegExp('^[a-zA-Z]*$');
		let emailRegex = RegExp('/^[a-zA-Z0-9.!#$%&' * +/=?^_`{|}~-]+@[A-Z0-9.-]+\.[A-Z]{2,}$'/);
		let passwordRegex = RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$');

		if (!this.state.userObj.firstName) {
			fnameErr = 'First name is required!';
		}
    if (this.state.userObj.firstName && !onlyLetterRegex.test(this.state.userObj.firstName)) {
      fnameErr = 'First name must contain only letters';
		}
		if (!this.state.userObj.lastName) {
      lnameErr = 'Last name is required!';
    }
    if (this.state.userObj.lastName && !onlyLetterRegex.test(this.state.userObj.lastName)) {
			lnameErr =  'Last name must contain only letters!';
    }
		if (!this.state.userObj.email) {
				emailErr = 'Email is required!';
		}
    if (this.state.userObj.email && !this.state.userObj.email.includes("@") && !emailRegex.test(this.state.userObj.email)) {
      emailErr = 'Invalid email format';
    }
    if (!this.state.userObj.password) {
      passwordErr =  'Password is required!';
    }
    if (this.state.userObj.password && !passwordRegex.test(this.state.userObj.password)) {
      passwordErr =  'Password must contain at least 8 character, one letter and one number!';
    }
    if (fnameErr || lnameErr || emailErr || passwordErr) {
      this.setState({ fnameErr, lnameErr, emailErr, passwordErr });
      return false;
    }
    return true;
  }

  onSubmit = () => {
    const isValid = this.validate();
    if (isValid) {
      axios.post(route.baseURL + route.users, this.state.userObj)
      .then(() => {
        this.setState({
          redirectAfterSubmit: true,
        })
      })
    }
  }

  render() {
    const { redirectAfterSubmit } = this.state;
    return (
      <div className="custom-container">
        <Container>
          <Row>
            <Col md="12" className="center-align" >
              <h4>Please enter the following data for register</h4>
              <hr/>
              <br />
            </Col>
          </Row>
          <Form id="formNewUsers">
            <Row>
              <Col md="6" className="center-align">
                <p>Required information*:</p>
                <InputGroup>
                  <Input placeholder="First name" name="firstName" onChange={ this.handleChange } />
                </InputGroup>
                <div className="required_field">{this.state.fnameErr}</div>
                <InputGroup>
                  <Input placeholder="Last name" name="lastName" onChange={ this.handleChange } />
                </InputGroup>
                <div className="required_field">{this.state.lnameErr}</div>
                <InputGroup>
                  <Input type="email" placeholder="Email" name="email" onChange={ this.handleChange } />
                  <InputGroupAddon addonType="append">@</InputGroupAddon>
                </InputGroup>
                <div className="required_field">{this.state.emailErr}</div>
                <InputGroup>
                  <Input type="password" placeholder="Password" name="password" onChange={ this.handleChange } />
                  <InputGroupAddon addonType="append">**</InputGroupAddon>
                </InputGroup>
                <div className="required_field">{this.state.passwordErr}</div>
              </Col>
              <Col md="6" className="center-align">
                <p> Optional information:</p>
                <InputGroup>
                  <Input placeholder="Hobby" name="hobby" onChange={ this.handleChange } />
                </InputGroup>
                <InputGroup>
                  <Input type="number" placeholder="Age" min="1" name="age" onChange={ this.handleChange } />
                </InputGroup>
              </Col>
            </Row>
            <Row>
              <Col md="12" className="center-position">
                <Button color="primary" className="submit-button"  disabled={!this.state.terms || !this.state.condition} onClick={() => this.onSubmit()} >Register</Button>
              </Col>
            </Row>
            <Row>
              <Col md="12">
                <div>
                <Input type="checkbox" name="terms" id="terms"  onChange={ this.handleChange } /> I agree with terms <br/>
                </div>
              </Col>
              <Col md="12">
                <div>
                  <Input type="checkbox" name="condition" id="condition"  onChange={ this.handleChange } /> I agree with condition
                </div>
              </Col>
            </Row>
            { redirectAfterSubmit && (
              <Redirect to='/books' />
            )}
          </Form>
        </Container>
      </div>
    );
  }
}
export default NewUser;
