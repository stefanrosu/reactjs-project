import React, { Component } from 'react'
import './courses.scss'
import route from '../../api-services/index';
import axios from 'axios';
import TextTruncate from 'react-text-truncate';
import {Link} from 'react-router-dom'

import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
  Form,
  Input,
  InputGroup
} from 'reactstrap';

class Courses extends Component {
    constructor(props){
        super(props)
        this.state = {
          allCourses: [],
          minPage: 0,
          maxPage: 4,
          searchValue: '',
          searchErrMessage: '',
        }
    }

  componentDidMount = () => {

    if (this.props.match.path === '/courses') {
      axios.get(route.baseURL + route.coursesLimitTo + '&_start='+ this.state.minPage + '&_end=' + this.state.maxPage)
        .then(res => {
        this.setState({
          allCourses: res.data,
        })
      })
    }

    if (this.props.match.path === '/courses/:bookId') {
      axios.get(route.baseURL + route.coursesByBooksID + this.props.match.params.bookId + '&_start='+ this.state.minPage + '&_end=' + this.state.maxPage)
      .then(res => {
      this.setState({
        allCourses: res.data,
       })
      })
    }
  }

  getMore = () => {
    this.setState({
      maxPage: this.state.maxPage + 4,
    }, () => {
      this.componentDidMount();
    })
  }

  getLess = () => {
    if (this.state.maxPage > 4) {
      this.setState({
        maxPage: this.state.maxPage - 4,
      }, () => {
        this.componentDidMount();
      })
    }
  }


  handleSearchChange = (event) => {
    this.setState({
      searchValue: event.target.value,
      searchErrMessage: '',
    })
  }

  searchField = () => {
    if (this.state.searchValue !== '') {
      axios.get(route.baseURL + route.coursesLimitTo + '&q=' + this.state.searchValue)
    .then(res => {
      this.setState({
        allCourses: res.data,
      })
    })
    }
    else {
      this.setState({
        searchErrMessage: 'Please enter a value before search!'
      })
    }
  }

  resetSearchField = () => {
    this.setState({
      searchValue: '',
    }, () => {
      axios.get(route.baseURL + route.coursesLimitTo + '&q=' + this.state.searchValue)
      .then(res => {
        this.setState({
          allCourses: res.data,
          searchErrMessage: '',
        })
      })
      document.getElementById('searchForm').reset()
    })
  }

  render() {
    const { allCourses, maxPage } = this.state;
      return (
        <>
        <Container className="custom-container">
          <Row>
            <Col md="12" className="center-position" >
              <h2>Enjoy all available courses</h2>
              <br />
                <Form id="searchForm">
                  <Row>
                    <Col md="7">
                    <InputGroup>
                        <Input type="search" placeholder="... type to search" name="searchValue" onChange={this.handleSearchChange} />
                      </InputGroup>
                    <div className="required_field">{ this.state.searchErrMessage }</div>
                    </Col>
                    <Col md="5" >
                      <Button color="primary" size="md" onClick={ ()=> this.searchField() } >Find your course</Button>
                      <Button size="md" onClick={()=> this.resetSearchField()} >Reset</Button>
                    </Col>
                  </Row>
                </Form>
            </Col>
          </Row>
          <hr />
          <Row>
            {allCourses.map((item) => (
            <Col md="6" key={item.id}>
              <Card >
              <CardImg top width="300px" height="300px" src={item.images} alt="Card image" />
              <CardBody>
                <CardTitle>
                  <b>Title:</b> {item.small_description}
                </CardTitle>
                  <b>Description:</b>
                  <TextTruncate
                  line={5}
                  truncateText="…"
                  text={item.long_description}
                  />
                <CardText>
                  <b>Tags:</b> {item.tags}
                </CardText>
                <CardText>
                  <b>Points:</b> {item.points}
                </CardText>
              </CardBody>
              </Card>
              <br/>
              </Col>
            ))}
              <Col md="12" className="center-position">
                <Button color="success" onClick={() => { this.getMore() }}>
                  Show more
                </Button>
              {maxPage > 4 && (
                <Button color="danger" onClick={() => { this.getLess() }} >
                  Show less
                </Button>
            )}
            </Col>
          </Row>
          <hr/>
          <Row>
            <Col md="12" className="center-position">
              <Link to="/books">
                <Button color="info" >Back to books</Button>
              </Link>
            </Col>
          </Row>
          <br/>
        </Container>
        </>
    )
  }
}

export default Courses;
