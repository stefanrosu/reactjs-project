import React, {Component} from 'react';
import './header.scss';
export default class HeaderItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentTime: null,
    }
  }

  componentDidMount() {
    setInterval( () => {
      this.setState({
        currentTime : new Date().toLocaleString()
      })
    },1000)
  }

  render() {
    const { currentTime } = this.state;
      return (
        <>
          <header className="header-body" id="top"></header>
          <h1 className="header-title"> Books store </h1>
          <div className="datetime"> {currentTime} </div>
        </>
    )
  }
}
