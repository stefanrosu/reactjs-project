import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Location from '../public-components/location-component';
import Home from '../public-components/home-component';
import AllUsers from '../admin-components/all-users-component';
import Books from '../public-components/books-component';
import Courses from '../public-components/courses-component';
const Main = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/home" component={Home} />
    <Route exact path="/location" component={Location} />
    <Route exact path="/users" component={AllUsers} />
    <Route exact path="/books" component={Books} />
    <Route exact path="/courses" component={Courses} />
    <Route exact path="/courses/:bookId" component={Courses} />
  </Switch>
)
export default Main;
