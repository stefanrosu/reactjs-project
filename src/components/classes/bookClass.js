const BookClass = {
  userId: null,
  title: '',
  description: '',
  author: '',
  publisher: '',
  published: null,
  pages: null,
  website: '',
  image: '',
}
export default BookClass;
