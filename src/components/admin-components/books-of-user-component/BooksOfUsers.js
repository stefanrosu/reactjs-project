import React, { Component } from 'react';
import './BooksOfUsers.scss';
import route from '../../api-services/index';
import axios from 'axios';
import { Button } from 'reactstrap';
import Moment from 'react-moment';
import Popup from "reactjs-popup";



class BooksOfUsers extends Component {
	constructor(props){
		super(props)
		this.state = {
			allBooks: [],
			minPageBooks: 0,
			maxPageBooks: 3,
			totalBooks: null,
		}
	}

	getBooksByUser = () => {
		axios.get(route.baseURL + route.booksByUserID + this.props.userId + '&_start=' + this.state.minPageBooks + '&_end=' + this.state.maxPageBooks)
			.then(res => {
				this.setState({
					allBooks: res.data,
					totalBooks: res.headers['x-total-count']
				},() => {
					window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
			})
    	})
	}

	getMoreBooks = () => {
		if(this.state.maxPageBooks <= this.state.totalBooks){
			this.maxPageBooks += 3;
			this.setState({
				maxPageBooks: this.state.maxPageBooks + 3,
			}, () => {
				this.getBooksByUser()
			})
    }
  }

  getLessBooks = () => {
    if (this.state.maxPageBooks > 3) {
      this.setState({
        maxPageBooks: this.state.maxPageBooks - 3,
      }, () => {
        this.getBooksByUser()
      })
    }
  }

	prepareForDelete = (item) => {
		this.props.deletingBook(item);
		const allBooksUpdated = this.state.allBooks;
		allBooksUpdated.splice(allBooksUpdated.indexOf(item), 1);
		this.setState({
			allBooks: allBooksUpdated
		})
	}

	render() {
		let { allBooks, maxPageBooks, totalBooks } = this.state;
		return (
			<>
				<h5>The list of books<i className="fa fa-plus-circle custom-add" onClick={() => this.props.openAddBook()} ></i> </h5>
				{(Number(totalBooks) === 0 &&
					<h5> No books found!</h5>
				)}
				<ul className="fixed-panel border-right">
					{allBooks.map(item => (
						<div key={item.id}>
							<li><span>Title:</span> {item.title}</li>
							<li><span>Author:</span> {item.author}</li>
							<li><span>Publisher:</span> {item.publisher}</li>
							<li><span>Published: </span>
								<Moment format="DD/MM/YYYY">
									{item.published}
								</Moment>
							</li>
							<li><span>Pages:</span> {item.pages}</li>
							<li>
								<span>
									<i className="fa fa-cog custom-settings"></i>
									<i className="fa fa-long-arrow-right custom-long-arrow"></i>
								</span>
								<Popup trigger={<i className="fa fa-trash custom-trash-books" ></i>} position="right center">
								{close => (
								<div className="popup-container">
									<p>Do you want to delete this book?</p>
									<Button size="sm" color="danger" onClick={close} >No</Button>
									<Button size="sm" color="primary"  className="show-buttons" onClick={() => this.prepareForDelete(item)} >Yes</Button>
									</div>
									)}
								</Popup>
							</li>
							<li className="view_courses" onClick={() => this.props.loadingCourse(item)} >Click to view courses</li>
							<br />
						</div>
					))}
				{maxPageBooks > 3 && (
					<Button color="danger" size="sm" className="show-buttons" onClick={() => { this.getLessBooks() }} >
						Show less
				</Button>)}

				{ maxPageBooks < totalBooks && (
					<Button color="success" size="sm" className="show-buttons" onClick={() => { this.getMoreBooks() }}>
						Show more
				</Button>)}
				</ul>
			</>
		)
	}
}

export default BooksOfUsers
