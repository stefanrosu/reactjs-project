import React, { Component } from 'react';
import  './users.scss'
import route from '../../api-services/index';
import axios from 'axios';
import Popup from "reactjs-popup";
import { Container, Row, Col, Table, Button, Form, Input, InputGroup } from 'reactstrap';
import EditUser from '../edit-user-component/EditUser';
import AddBook from '../add-book-component/AddBook';
import BooksOfUser from '../books-of-user-component/BooksOfUsers';
import CoursesOfBook from '../courses-of-book-component/CoursesOfBook';
import UserClass from '../../classes/userClass';
import BookClass from '../../classes/bookClass';

export default class AllUsers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      allUsers: [],
      allBooks: [],
      allCourses: [],
      userObj: UserClass,
      bookObj: BookClass,
      book_title: '',
      displayEditform: false,
      displayBooks: false,
      displayAddBooks: false,
      displayCourses: false,
      userId: null,
      bookId: null,
      searchValue: '',
      searchErrMessage: '',
      usersPerPage: 5,
      totalUsers: null,
      paginationArr: [],
    }
  }

  async getAllUsers () {
    try {
      const result = await axios.get(route.baseURL + route.usersLimitTo + '_start=0&_end=' + this.state.usersPerPage);
      let totalCount = result.headers['x-total-count'];
      this.setState({
        allUsers: result.data,
        totalUsers: totalCount,
        paginationArr: [Math.round(totalCount/4), Math.round(totalCount/2), totalCount]
      })
    } catch (error) {
      console.log('ERR: ', error);
    }
  }

  componentDidMount() {
    this.getAllUsers();
  }

  viewBooks = (item) => {
    this.setState({
      displayEditform: false,
      displayCourses: false,
      displayAddBooks: false,
      displayBooks: true,
      userId: item.id,
    }, () => {
      this.refs.bookChildFunctions.getBooksByUser()
    })
  }

  viewCourses = (item) => {
    this.setState({
      displayCourses: true,
      displayEditform: false,
      displayAddBooks: false,
      bookId: item.id,
      book_title: item.title,
    }, () => {
        this.refs.courseChildFunctions.getCoursesByBook();
    })
  }

  onDeleteUser = (item) => {
    axios.delete(route.baseURL + route.users + item.id)
      .then(() => {
      const allUsersUpdated = this.state.allUsers;
      allUsersUpdated.splice(allUsersUpdated.indexOf(item), 1);

      this.setState({
        allUsers: allUsersUpdated,
        displayBooks: false,
        displayAddBooks: false,
        displayCourses: false,
      })
     })
  }

  onDeleteCourse = (item) => {
    const allCoursesUpdated = this.state.allCourses;
    allCoursesUpdated.splice(allCoursesUpdated.indexOf(item), 1);

    axios.delete(route.baseURL + route.courses + item.id)
      .then(() => {
      this.setState({
        allCourses: allCoursesUpdated,
        displayAddBooks: false,
      })
     })
  }

  onDeleteBook = (item) => {
    axios.delete(route.baseURL + route.books + item.id)
      .then(() => {
      this.setState({
        displayCourses: false,
        displayAddBooks: false,
      })
     })
  }

  addBookForm = () => {
    this.setState({
      displayCourses: false,
      displayAddBooks: true,
    })
  }

  saveBook = (item) => {
    axios.post(route.baseURL + route.books, item).then(() => {
      this.setState({
        displayAddBooks: false,
      })
    })
  }

  onEditUser = (item) => {
    this.setState({
      userObj: item,
      displayEditform: true,
      displayBooks: false,
      displayAddBooks: false,
    }, () => {
        window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    })
  }

  updateUser = (itemFromChild) => {
    axios.put(route.baseURL + route.users + this.state.userObj.id, itemFromChild)
    .then(() => {
      this.getAllUsers();
      this.setState({
        displayEditform: false,
      })
    })
  }

  cancel = () => {
    this.setState({
      displayEditform: false,
      displayAddBooks: false,
    })
  }

  handlePagination = e => {
    this.setState({
      usersPerPage: e.target.value
    }, () => {
      this.getAllUsers();
    })

  }

  handleChange = e => {
    this.setState({
      userObj: {
        ...this.state.userObj,
        [e.target.name]: e.target.value
      }
    });
  }

  handleSearchChange = (event) => {
    this.setState({
      searchValue: event.target.value,
      searchErrMessage: '',
    })
  }

  searchField = () => {
    if (this.state.searchValue !== '') {
      axios.get(route.baseURL + route.usersLimitTo + '&q=' + this.state.searchValue)
      .then(res => {
        this.setState({
          allUsers: res.data,
        })
      })
    }
    else {
      this.setState({
        searchErrMessage: 'Please enter a value before search!'
      })
    }
  }

  resetSearchField = () => {
    this.setState({
      searchValue: '',
    }, () => {
      axios.get(route.baseURL + route.usersLimitTo + '_start=0&_end=5&q=')
      .then(res => {
        this.setState({
          allUsers: res.data,
        })
        document.getElementById('searchForm').reset()
      })
    })
  }

  render() {
    let {
      allUsers,
      allCourses,
      displayEditform,
      displayBooks,
      displayAddBooks,
      displayCourses,
      book_title,
      userObj,
      userId,
      bookObj,
      bookId,
      paginationArr
    } = this.state;
    return (
      <>
        <Container className="custom-container">
          <Row>
            <Col md="12" className="center-position">
              <h3>The list of all users from database</h3>
              <br />
              <Form id="searchForm" onSubmit={this.searchField} >
                <Row>
                  <Col md="7">
                  <InputGroup>
                      <Input type="search" placeholder="... type to search" name="searchValue" onChange={this.handleSearchChange} />
                    </InputGroup>
                  <div className="required_field">{ this.state.searchErrMessage }</div>
                  </Col>
                  <Col md="5" >
                    <Button color="primary" size="md" onClick={ ()=> this.searchField() } >Find your user</Button>
                    <Button size="md" onClick={()=> this.resetSearchField()} >Reset</Button>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
          <br />
          <Row>
            <Col md="12" className="custom-height" >
            <Table responsive >
              <thead>
                <tr>
                  <th>#</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Hobby</th>
                  <th>Age</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {allUsers.map(item => (
                  <tr key={item.id}>
                    <th>{item.id}</th>
                    <td>{item.firstName}</td>
                    <td>{item.lastName}</td>
                    <td>{item.email}</td>
                    <td>{item.hobby}</td>
                    <td>{item.age}</td>
                    <td>
                      <i className="fa fa-eye custom-view-users" onClick={() => this.viewBooks(item)} ></i>
                      <i className="fa fa-edit custom-edit-users" onClick={() => this.onEditUser(item)} ></i>
                      <Popup trigger={<i className="fa fa-trash custom-trash-users" ></i>} position="left center">
                        {close => (
                        <div className="popup-container">
                          <p>Do you want to delete this user?</p>
                          <Button size="sm" color="danger" onClick={close} >No</Button>
                          <Button size="sm" color="primary"  className="show-buttons" onClick={() => this.onDeleteUser(item)} >Yes</Button>
                          </div>
                          )}
                    </Popup>
                    </td>
                  </tr>
                ))}
              </tbody>
              </Table>
              </Col>
          </Row>
          <hr />
          <Row className="float-right" >
            <Col md="12" className="pagination-container d-flex" >
            <label className="show-pagination-label">Show</label>
              <select className="form-control pagination-select" onChange={ this.handlePagination } >
                {paginationArr.map(item => (
                  <option key={item} value={item} >
                    {item}
                  </option>
                ))}
              </select>
            </Col>
          </Row>
          {displayEditform && (
            <EditUser
              userData={userObj}
              updatingUser={this.updateUser.bind(this)}
              cancel={this.cancel}
            />
          )}

          {(displayBooks &&
            <>
            <Row>
              <Col md="5">
                <BooksOfUser
                  userId={userId}
                  deletingBook={this.onDeleteBook.bind(this)}
                  openAddBook={this.addBookForm}
                  loadingCourse={this.viewCourses.bind(this)}
                  ref="bookChildFunctions"
                />
              </Col>

              {(displayCourses &&
                <Col md="7">
                  <CoursesOfBook
                    bookId={bookId}
                    bookTitle={book_title}
                    courseData={allCourses}
                    deletingCourse={this.onDeleteCourse.bind(this)}
                    ref="courseChildFunctions"
                  />
              </Col>
              )}

              {(displayAddBooks &&
                <Col md="6" className="center-align" >
                  <AddBook
                    bookData={bookObj}
                    userId={userId}
                    savingBook={this.saveBook.bind(this)}
                    cancel={this.cancel}
                  />
                </Col>
              )}

            </Row>
            </>
            )}
        </Container>
      </>
    );
  }
}
