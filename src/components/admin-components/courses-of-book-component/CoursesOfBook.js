import React, { Component } from 'react';
import './CoursesOfBook.scss';
import route from '../../api-services/index';
import axios from 'axios';
import { Col, Button } from 'reactstrap';
import Popup from "reactjs-popup";


class CoursesOfBook extends Component {
	constructor(props){
		super(props)
		this.state = {
			allCourses: [],
			minPageCourses: 0,
			maxPageCourses: 3,
			totalCourses: null,
		}
	}

	getCoursesByBook = () => {
		axios.get(route.baseURL + route.coursesByBooksID + this.props.bookId + '&_start=' + this.state.minPageCourses + '&_end=' + this.state.maxPageCourses)
		.then(res => {
			this.setState({
				allCourses: res.data,
				totalCourses: res.headers['x-total-count']
			}, () => {
				window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
			})
		})
  }

	getMoreCourses = () => {
		if(this.state.maxPageCourses <= this.state.totalCourses){
			this.maxPageCourses += 3;
			this.setState({
				maxPageCourses: this.state.maxPageCourses + 3,
			}, () => {
				this.getCoursesByBook()
			})
    }
  }

  getLessCourses = () => {
    if (this.state.maxPageCourses > 3) {
      this.setState({
        maxPageCourses: this.state.maxPageCourses - 3,
      }, () => {
        this.getCoursesByBook()
      })
    }
  }

	render() {
		const { allCourses, maxPageCourses, totalCourses } = this.state;
		return (
			<>
				<h5>The courses of book: {this.props.bookTitle} </h5>
				{(Number(totalCourses) === 0 &&
					<h5> No courses found!</h5>
				)}
				<ul className="fixed-panel">
				{allCourses.map(item => (
					<Col md="12" key={item.id}>
						<li>
							<span>Title:</span> {item.small_description}
						</li>
						<li>
							<span>Tags:</span> {item.tags}
						</li>
						<li>
							<span>Points:</span> {item.points}
						</li>
						<li>
						<img className="courses_img" src={item.images} alt="Course" /> <br />
							<span>
								<i className="fa fa-cog custom-settings"></i>
								<i className="fa fa-long-arrow-right custom-long-arrow"></i>
							</span>
							<Popup trigger={<i className="fa fa-trash custom-trash-books" ></i>} position="right center">
							{close => (
							<div className="popup-container">
								<p>Do you want to delete this course?</p>
								<Button size="sm" color="danger" onClick={close} >No</Button>
								<Button size="sm" color="primary"  className="show-buttons" onClick={() => this.props.deletingCourse(item)} >Yes</Button>
								</div>
								)}
							</Popup>
					</li>
					<br/>
					</Col>
				))}

				{maxPageCourses > 3 && (
					<Button color="danger" size="sm" className="show-buttons" onClick={() => this.getLessCourses() } >
					Show less
					</Button>
				)}

				{maxPageCourses < totalCourses && (
					<Button color="success" size="sm" className="show-buttons" onClick={() =>  this.getMoreCourses() }>
					Show more
					</Button>
				)}
				</ul>
			</>
		)
	}
}

export default CoursesOfBook
