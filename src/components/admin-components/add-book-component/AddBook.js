import React, { Component } from 'react'
import {  Row, Col, Button, Form, Input, InputGroup } from 'reactstrap';
import './AddBook.scss';

class AddBook extends Component {
	constructor(props){
		super(props)
		this.state = {
			bookObj: this.props.bookData,
		}
	}

	handleChange = e => {
		this.setState({
			bookObj: {
				...this.state.bookObj,
				[e.target.name]: e.target.value,
				userId: this.props.userId,
			}
		});
	}

	prepareForPost = () => {
		this.props.savingBook(this.state.bookObj)
	}

	render(){
		return (
			<>
				<h5>Add your new book</h5>
				<hr />
				<br />
				<Form id="formNewBook">
					<InputGroup>
						<Input placeholder="Title" name="title" value={this.state.bookObj.title || '' } onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.fnameErr}</div> */}
					<InputGroup>
						<Input type="textarea" placeholder="Description" name="description" value={ this.state.bookObj.description || ''} onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.fnameErr}</div> */}
					<InputGroup>
						<Input placeholder="Author" name="author" value={ this.state.bookObj.author || ''} onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.lnameErr}</div> */}
					<InputGroup>
						<Input placeholder="Publisher" name="publisher" value={ this.state.bookObj.publisher || ''} onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.lnameErr}</div> */}
					<InputGroup>
						<Input type="date" placeholder="Published" name="published" value={ this.state.bookObj.published || ''} onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.lnameErr}</div> */}
					<InputGroup>
						<Input type="number" placeholder="Pages" name="pages" value={ this.state.bookObj.pages || ''} onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.lnameErr}</div> */}
					<InputGroup>
						<Input placeholder="Paste your book website link" name="website" value={ this.state.bookObj.website || ''} onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.lnameErr}</div> */}
					<InputGroup>
						<Input placeholder="Paste your book image link" name="image" value={ this.state.bookObj.image || ''} onChange={event => this.handleChange(event)} />
					</InputGroup>
					{/* <div className="required_field">{this.state.lnameErr}</div> */}
				<Row>
					<Col md="12" className="center-position">
						<Button color="danger" className="submit-button" onClick={() => this.props.cancel()} >Cancel</Button>
						<Button color="primary" className="submit-button" onClick={() => this.prepareForPost()} >Save</Button>
					</Col>
				</Row>
			</Form>

			</>
		)
	}
}

export default AddBook
