import React, { Component } from 'react';
import { Row, Col, Button, Form, Input, InputGroup, InputGroupAddon } from 'reactstrap';

class EditUser extends Component {
	constructor(props){
		super(props)
		this.state = {
			userObj: this.props.userData,
			fnameErr: '',
      lnameErr: '',
      emailErr: '',
      passwordErr: '',
		}
	}

	componentDidUpdate(oldProps) {
		const newProps = this.props.userData;
		if(oldProps.userData !== newProps) {
			this.setState({
				userObj: newProps,
			})
		}
	}

	handleChange = e => {
		this.setState({
			userObj: {
				...this.state.userObj,
				[e.target.name]: e.target.value
			}
		});
	}

	validate = () => {
    let fnameErr = '';
    let lnameErr = '';
    let emailErr = '';
		let passwordErr = '';

		let onlyLetterRegex = RegExp('^[a-zA-Z]*$');
		let emailRegex = RegExp('/^[a-zA-Z0-9.!#$%&' * +/=?^_`{|}~-]+@[A-Z0-9.-]+\.[A-Z]{2,}$'/);
		let passwordRegex = RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$');

		if (!this.state.userObj.firstName) {
			fnameErr = 'First name is required!';
		}
    if (this.state.userObj.firstName && !onlyLetterRegex.test(this.state.userObj.firstName)) {
      fnameErr = 'First name must contain only letters';
		}
		if (!this.state.userObj.lastName) {
      lnameErr = 'Last name is required!';
    }
    if (this.state.userObj.lastName && !onlyLetterRegex.test(this.state.userObj.lastName)) {
			lnameErr =  'Last name must contain only letters!';
    }
		if (!this.state.userObj.email) {
				emailErr = 'Email is required!';
		}
    if (this.state.userObj.email && !this.state.userObj.email.includes("@") && !emailRegex.test(this.state.userObj.email)) {
      emailErr = 'Invalid email format';
    }
    if (!this.state.userObj.password) {
      passwordErr =  'Password is required!';
    }
    if (this.state.userObj.password && !passwordRegex.test(this.state.userObj.password)) {
      passwordErr =  'Password must contain at least 8 character, one letter and one number!';
    }
    if (fnameErr || lnameErr || emailErr || passwordErr) {
      this.setState({ fnameErr, lnameErr, emailErr, passwordErr });
      return false;
    }
    return true;
  }

	checkBeforeUpdate = () => {
		const isValid = this.validate();
		if (isValid) {
			this.props.updatingUser(this.state.userObj)
		}
	}

	render() {
		return (
			<>
				<h4>Edit user </h4>
				<Form id="formEditUsers">
					<Row>
						<Col md="6" className="center-align">
							<p>Required information*:</p>
							<InputGroup>
								<Input placeholder="First Name" name="firstName" value={this.state.userObj.firstName || ''} onChange={ this.handleChange } />
							</InputGroup>
							<div className="required_field">{this.state.fnameErr}</div>
							<InputGroup>
								<Input placeholder="Last name" name="lastName" value={this.state.userObj.lastName || ''} onChange={ this.handleChange } />
							</InputGroup>
							<div className="required_field">{this.state.lnameErr}</div>
							<InputGroup>
								<Input type="email" placeholder="email" name="email" value={this.state.userObj.email || ''} onChange={ this.handleChange } />
								<InputGroupAddon addonType="append">@</InputGroupAddon>
							</InputGroup>
							<div className="required_field">{this.state.emailErr}</div>
							<InputGroup>
								<Input type="password" placeholder="Password" name="password" value={this.state.userObj.password || ''} onChange={ this.handleChange } />
								<InputGroupAddon addonType="append">**</InputGroupAddon>
							</InputGroup>
							<div className="required_field">{this.state.passwordErr}</div>
						</Col>
						<Col md="6" className="center-align">
							<p> Optional information:</p>
							<InputGroup>
								<Input placeholder="hobby" name="hobby" value={this.state.userObj.hobby || ''} onChange={ this.handleChange } />
							</InputGroup>
							<InputGroup>
								<Input type="number" placeholder="age" min="1" name="age" value={this.state.userObj.age || ''} onChange={ this.handleChange } />
							</InputGroup>
						<br />
						<Button color="danger" size="sm" onClick={() => this.props.cancel()} > Cancel</Button>&nbsp;
						<Button color="primary" size="sm" onClick={() => this.checkBeforeUpdate()} >Update changes</Button>
						</Col>
						</Row>
					</Form>
				<br />
			</>
		)
	}
}

export default EditUser
