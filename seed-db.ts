import * as fs from 'fs';
import * as faker from 'faker';

const dbFileName = 'db.json';

const maxUsers = 10;
const maxBooks = 5;
const maxCourses = 5;

class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    hobby: string;
    age: number;
}

class Book {
    id: number;
    userId: number;
    title: string;
    author: string;
    published: Date;
    publisher: string;
    pages: number;
    description: string;
    website: string;
    images: string;
}

class Course {
    id: number;
    bookId: number;
    small_description: string;
    long_description: string;
    tags: string;
    points: number;
    images: string;
}
class DB {
    users: Array<User>;
    books: Array<Book>;
    courses: Array<Course>;
}

export default class SeedDB {
    private db: DB = new DB();

    constructor() {
        this.db.users = [];
        this.db.books = [];
        this.db.courses = [];
    }

    private randomDate(start, end): Date {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }

    private seedUsers(usersNumber: number): void {
        for (let index = 1; index <= usersNumber; index++) {
            let user: User = {
                "id": index,
                "firstName": faker.name.firstName(),
                "lastName": faker.name.lastName(),
                "email": faker.internet.email(),
                "password": faker.internet.password(),
                "hobby": faker.random.words(),
                "age": Math.floor((Math.random() * 70) + 18)
            };

            this.db.users.push(user);
        }
    }

    private seedBooks(booksNumber: number): void {
        let bookId = 1;
        this.db.users.forEach(user => {
            for (let index = 1; index <= booksNumber; index++) {
                let book: Book = {
                    "id": bookId,
                    "userId": user.id,
                    "title": faker.lorem.words(),
                    "author": faker.name.findName(),
                    "published": faker.date.past(),
                    "publisher": faker.company.companyName(),
                    "pages": Math.floor((Math.random() * 1000) + 10),
                    "description": faker.lorem.paragraphs(),
                    "website": faker.internet.url(),
                    "images": faker.image.imageUrl()
                };
                this.db.books.push(book);
                bookId++;
            }
        })
    }

    private seedCourses(coursesNumber: number): void {
        let courseId = 1;
        this.db.books.forEach(book => {
            for (let index = 1; index <= coursesNumber; index++) {
                let course: Course = {
                    "id": courseId,
                    "bookId": book.id,
                    "small_description": faker.lorem.sentence(),
                    "long_description": faker.lorem.paragraphs(),
                    "tags": faker.random.word(),
                    "points": Math.floor((Math.random() * 20) + 1),
                    "images": faker.image.imageUrl()
                };

                this.db.courses.push(course);
                courseId++;
            }
        });
    }

    public seedDB(): void {
        this.seedUsers(maxUsers);
        this.seedBooks(maxBooks);
        this.seedCourses(maxCourses);

        let json = JSON.stringify(this.db);
        fs.writeFile(dbFileName, json, 'utf8', (err) => {
            if (err) throw err;
            console.log('Seed complete!');
        });
    }
};

let seed: SeedDB = new SeedDB();
seed.seedDB();
